let classRemove = () =>{
    if (document.querySelector('.blue')){
        document.querySelector('.blue').classList.remove('blue');
    }
};

document.addEventListener("keydown", function(e){
    if (e.code === 'Enter'){
        classRemove();
        document.getElementById('btnEnter').classList.add('blue');
    }
    if (e.code === 'KeyS'){
        classRemove();
        document.getElementById('btnS').classList.add('blue');
    }
    if (e.code === 'KeyE'){
        classRemove();
        document.getElementById('btnE').classList.add('blue');
    }
    if (e.code === 'KeyO'){
        classRemove();
        document.getElementById('btnO').classList.add('blue');
    }
    if (e.code === 'KeyN'){
        classRemove();
        document.getElementById('btnN').classList.add('blue');
    }
    if (e.code === 'KeyL'){
        classRemove();
        document.getElementById('btnL').classList.add('blue');
    }
    if (e.code === 'KeyZ'){
        classRemove();
        document.getElementById('btnZ').classList.add('blue');
    }
});
